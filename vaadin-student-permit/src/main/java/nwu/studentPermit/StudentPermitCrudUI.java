package nwu.studentPermit;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.AbstractLayout;
import com.vaadin.ui.UI;
import nwu.ac.za.framework.SinglePageCRUDUI;
import nwu.ac.za.framework.VaadinUIUtility;
import nwu.ac.za.framework.context.SessionContext;
import nwu.studentPermit.config.ApplicationConfig;
import nwu.studentPermit.events.ShowNotificationEvent;
import nwu.studentPermit.service.PersonProxyService;
import nwu.studentPermit.ui.views.StudentPermitMainView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.vaadin.viritin.layouts.MVerticalLayout;

import java.util.Locale;

/**
 * This UI is the application entry point. A UI may either represent a browser window
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be
 * overridden to add component to the user interface and initialize non-component functionality.
 * <p>
 */
@Theme("nwu-default")
@SpringUI
public class StudentPermitCrudUI extends SinglePageCRUDUI {
    private final Logger log = LoggerFactory.getLogger(StudentPermitCrudUI.class.getName());

    @Autowired
    private ApplicationConfig appConfig;
    @Autowired
    private EventBus eventBus;
    @Autowired
    private StudentPermitMainView studentPermitMainView;
    @Autowired
    private PersonProxyService personProxyService;

    @Override
    public AbstractLayout getDetailContent() {
        eventBus.register(this);
        // There are a problem with the framework because we set our correct Language in newSessionContext()
        // but it is overridden by the time we call getSessionContext(). - Have to this to override the language after creation

        getSessionContext().setLang(getScreenLanguage());
        studentPermitMainView.setSessionContext(getSessionContext());
        studentPermitMainView.initTheView();

        detailContent = new MVerticalLayout(studentPermitMainView).withSpacing(false).withMargin(false);
        return detailContent;
    }

    private String getScreenLanguage() {
        Locale uiLocale = UI.getCurrent().getLocale();
        if (uiLocale.getLanguage().equals(VaadinUIUtility.SYSTEM_LANG_AF)) {
            return VaadinUIUtility.TYPE_KEY_SYSTEM_LANGUAGE_AF;
        } else {
            return VaadinUIUtility.TYPE_KEY_SYSTEM_LANGUAGE_EN;
        }
    }

    @Override
    public void afterUISetup() {
        studentPermitMainView.loadUserData();
    }

    @Override
    public void save() {
    }

    @Override
    public String getBrowserTabTitle() {
        return "student-permit";
    }

    @Override
    public SessionContext newSessionContext() {
        SessionContext sessionContext = new SessionContext();
        sessionContext.setLookupUser(lookupUser);
        sessionContext.setAuthenticatedUser(authenticatedUser);
        sessionContext.setLang(getScreenLanguage());
        return sessionContext;
    }

    public static StudentPermitCrudUI getCurrent() {
        return (StudentPermitCrudUI) UI.getCurrent();
    }

    @Subscribe
    public void showNotification(ShowNotificationEvent event) {
        if (event.getTranslate()) {
            setNotificationMsg(event.getErrorKey(), event.getSeverityType(), true);
        } else {
            setNotificationMsg(event.getErrorMessage(), event.getSeverityType(), false);
        }
    }
}
