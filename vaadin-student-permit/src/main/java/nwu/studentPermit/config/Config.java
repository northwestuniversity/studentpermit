package nwu.studentPermit.config;

import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.PropertyResolver;
import org.springframework.stereotype.Component;

@Component
@RefreshScope
public class Config {
    private final String ENVIRONMENT = "runtimeEnvironment";
    private final String CAS_ENABLED = "casEnabled";
    private final String APPLICATION_NAME = "application.name";
    private final String APPLICATION_VERSION = "application.version";
    private final String WEBSERVICE_DATABASE = "webservice.database";

    private final String SUPPORTING_SERVICE_API_VERSION = "supporting.service.api.version";
    private final String WS_SUPPORTING_SERVICE_READ_USER_NAME = "supporting_service_read_username";
    private final String WS_SUPPORTING_SERVICE_READ_PASSWORD = "supporting_service_read_password";
    private final String WS_SUPPORTING_SERVICE_WRITE_USER_NAME = "supporting_service_write_username";
    private final String WS_SUPPORTING_SERVICE_WRITE_PASSWORD = "supporting_service_write_password";

    private final String IAPI_VERSION = "identity.api.version";
    private final String IAPI_READ_USERNAME = "iapiapp_read_username";
    private final String IAPI_READ_PASSWORD = "iapiapp_read_password";

    private final String NOTIFICATIONCOMP_API_VERSION = "notification-comp.api.version";
    private final String WS_NOTIFICATIONCOMP_USER_NAME = "notificationcomp_read_username";
    private final String WS_NOTIFICATIONCOMP_PASSWORD = "notificationcomp_read_password";

    private String iAPIVersion;
    private String iAPIUsername;
    private String iAPIPassword;

    @Autowired
    private PropertyResolver propertyResolver;

    public String getCaseEnabled() {
        return propertyResolver.getProperty(CAS_ENABLED);
    }

    public String getEnviornment() {
        return propertyResolver.getProperty(ENVIRONMENT);
    }

    public String getConfigurationManagmentApiVersion() {
        return propertyResolver.getProperty(SUPPORTING_SERVICE_API_VERSION);
    }

    public String getConfigurationManagementReadUsername() {
        return propertyResolver.getProperty(WS_SUPPORTING_SERVICE_READ_USER_NAME);
    }

    public String getConfigurationManagementReadPassword() {
        return propertyResolver.getProperty(WS_SUPPORTING_SERVICE_READ_PASSWORD);
    }

    public String getConfigurationManagementWriteUsername() {
        return propertyResolver.getProperty(WS_SUPPORTING_SERVICE_WRITE_USER_NAME);
    }

    public String getConfigurationManagementWritePassword() {
        return propertyResolver.getProperty(WS_SUPPORTING_SERVICE_WRITE_PASSWORD);
    }

    public String getPermissionServiceReadUsername() {
        return propertyResolver.getProperty(WS_SUPPORTING_SERVICE_READ_USER_NAME);
    }

    public String getPermissionServiceReadPassword() {
        return propertyResolver.getProperty(WS_SUPPORTING_SERVICE_READ_PASSWORD);
    }

    public String getPermissionServiceApiVersion() {
        return propertyResolver.getProperty(SUPPORTING_SERVICE_API_VERSION);
    }

    public String getiAPIVersion() {
        if (Strings.isNullOrEmpty(iAPIVersion)) {
            iAPIVersion = propertyResolver.getProperty(IAPI_VERSION);
        }
        return iAPIVersion;
    }

    public String getiAPIUsername() {
        if (Strings.isNullOrEmpty(iAPIUsername)) {
            iAPIUsername = propertyResolver.getProperty(IAPI_READ_USERNAME);
        }

        return iAPIUsername;
    }

    public String getiAPIPassword() {
        if (Strings.isNullOrEmpty(iAPIPassword)) {
            iAPIPassword = propertyResolver.getProperty(IAPI_READ_PASSWORD);
        }

        return iAPIPassword;
    }

    public String getNotificationCompAPIVersion() {
        return propertyResolver.getProperty(NOTIFICATIONCOMP_API_VERSION);
    }

    public String getNotificationCompUserName() {
        return propertyResolver.getProperty(WS_NOTIFICATIONCOMP_USER_NAME);
    }

    public String getNotificationCompPassword() {
        return propertyResolver.getProperty(WS_NOTIFICATIONCOMP_PASSWORD);
    }

    public String getRuntimeEnvironment() {
        return propertyResolver.getProperty("runtimeEnvironment");
    }

    public String getAppVersion() {
        return propertyResolver.getProperty(APPLICATION_VERSION);
    }

    public String getAppName() {
        return propertyResolver.getProperty(APPLICATION_NAME);
    }

    public String getAppMajorVersion() {
        String appVersion = getAppVersion();
        String[] output = appVersion.split("\\.");
        return output[0];
    }

    public String getWebServiceDatabase() {
        String env = getEnviornment();
        if (env.toUpperCase().equals("PROD")) {
            return null;
        }
        String db = propertyResolver.getProperty(WEBSERVICE_DATABASE);
        if (db == null) {
            db = "v_test";
        }
        return db;
    }
}
