package nwu.studentPermit.config;

import com.google.common.base.Strings;
import com.vaadin.spring.annotation.SpringComponent;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.studentPermit.service.ConfigurationProxyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.PropertyResolver;

import javax.annotation.PostConstruct;
import java.util.Map;

@SpringComponent
@RefreshScope
public class ApplicationConfig {
	private final Logger log = LoggerFactory.getLogger(ApplicationConfig.class.getName());
	private final String CAS_ENABLED = "CAS.ENABLED";
	private final String CAS_SERVER_URL = "CAS.SERVER.URL";
	private final String ENVIRONMENT_URL = "ENVIRONMENT.URL";
	private final String WEB_CONTEXT = "WEB.CONTEXT";
	private final String DEV_INITIAL_UNIV_NUMBER = "DEV.INITIAL.UNIV.NUMBER";
	public final static String VERSION_GLOBAL = "GLOBAL";
    private final String WS_IAPI_READ_USER_NAME = "ws_iapiapp_read_username";
    private final String WS_IAPI_READ_USER_NAME_PASSWORD = "ws_iapiapp_read_username_password";
	private final String ADMIN = "ADMIN";
	private final String DIY_ENG_URL = "DIY.SERVICE.URL";
	private final String DIY_AFR_URL = "DIY.AF.SERVICE.URL";

    @Autowired
	private ConfigurationProxyService configServiceProxy;

	@Autowired
	private Config config;

	@Autowired
	private PropertyResolver propertyResolver;

	private Map<String, String> appVersionConfiguration;
	private Map<String, String> appGlobalConfiguration;
	private String configPrefix;
	private String admin;

	@PostConstruct
	public void resetConfiguration() {

        String configAPIVersion = config.getConfigurationManagmentApiVersion();
        String configMajorVersion = ServiceRegistryLookupUtility.getMajorVersion(configAPIVersion);
		String applicationVersion = config.getAppVersion();
		String applicationMajorVersion = ServiceRegistryLookupUtility.getMajorVersion(applicationVersion);

		appVersionConfiguration = configServiceProxy.getApllicationVersionSpecificConfiguration();
		appGlobalConfiguration = configServiceProxy.getApplicationGlobalConfiguration();
	}

    public String getIApisReadUsername() {
        return propertyResolver.getProperty(WS_IAPI_READ_USER_NAME);
    }

    public String getIApisReadPassword() {
        return propertyResolver.getProperty(WS_IAPI_READ_USER_NAME_PASSWORD);
    }

	public String getRuntimeEnviroment() {
		String propertyValue = config.getRuntimeEnvironment();
		if (propertyValue == null) {
			log.error("Configuration for 'RUNTIME ENVIROMENT' is missing");
			throw new VaadinUIException("error.config.runtime.enviroment.missing", true);
		}
		return propertyValue;
	}


	public String getCaseEnabled() {
		// If version has cas.enabled property return it, else return global setting
		return getPropertyValue(CAS_ENABLED);
	}

	public String getCASServerUrl() {
		return getPropertyValue(CAS_SERVER_URL);
	}

	public String getEnvironmentUrl() {
		return getPropertyValue(ENVIRONMENT_URL);
	}

	public String getWebContext() {
		return getPropertyValue(WEB_CONTEXT);
	}


	private String getPropertyValue(String propertyKey) {
		if (appVersionConfiguration == null || appGlobalConfiguration == null) {
			resetConfiguration();
		}

		String lookupVersionKey = "/" + getConfigPrefix() + "/V" + config.getAppMajorVersion() + "/" + propertyKey;
		String propertyValue = appVersionConfiguration.get(lookupVersionKey.toUpperCase());
		if (propertyValue != null ) {
			return propertyValue;
		} else {
			String lookupGlobalKey = "/" + getConfigPrefix() + "/" + VERSION_GLOBAL + "/" + propertyKey;
			propertyValue = appGlobalConfiguration.get(lookupGlobalKey.toUpperCase());
			return propertyValue;
		}
	}

	public String getConfigPrefix() {
		if (configPrefix != null) {
			return configPrefix;
		}

		configPrefix = config.getRuntimeEnvironment() + "/" + config.getAppName();
		return configPrefix.toUpperCase();
	}

	/**
	 * Only return true if config exist and is equal to true, anything else will return false
	 */
	public String getDevInitialUnivNumber() {
		String propertyValue = getPropertyValue(DEV_INITIAL_UNIV_NUMBER);
		return propertyValue;
	}

	public String getAdmin() {
		if(Strings.isNullOrEmpty(admin)) {
			admin = getPropertyValue(ADMIN);
		}
		return admin;
	}

	public String getDIY_UrlEN() {
		return getPropertyValue(DIY_ENG_URL);
	}

	public String getDIY_UrlAF() {
		return getPropertyValue(DIY_AFR_URL);
	}
}
