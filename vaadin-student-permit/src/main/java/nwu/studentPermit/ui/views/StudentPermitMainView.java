package nwu.studentPermit.ui.views;

import ac.za.nwu.common.dto.MetaInfo;
import ac.za.nwu.core.person.dto.PersonAffiliationInfo;
import ac.za.nwu.core.person.dto.PersonBiographicInfo;
import ac.za.nwu.core.type.dto.TypeInfo;
import ac.za.nwu.core.type.dto.TypeNameInfo;
import assemble.edu.common.dto.ContextInfo;
import assemble.edu.exceptions.DoesNotExistException;
import assemble.edu.exceptions.InvalidParameterException;
import assemble.edu.exceptions.MissingParameterException;
import assemble.edu.exceptions.OperationFailedException;
import assemble.edu.exceptions.PermissionDeniedException;
import com.google.common.base.Strings;
import com.google.common.eventbus.EventBus;
import com.vaadin.data.HasValue;
import com.vaadin.server.UserError;
import com.vaadin.shared.ui.ContentMode;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import nwu.ac.za.framework.context.SessionContext;
import nwu.ac.za.framework.i18n.Translate;
import nwu.ac.za.framework.i18n.VaadinI18n;
import nwu.ac.za.ui.SeverityType;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.enabling.capabilities.supporting.service.question.dto.BusinessEntityQuestionAnswerInfo;
import nwu.enabling.capabilities.supporting.service.question.dto.QuestionInfo;
import nwu.enabling.capabilities.supporting.service.question.dto.QuestionnaireInfo;
import nwu.studentPermit.StudentPermitCrudUI;
import nwu.studentPermit.config.ApplicationConfig;
import nwu.studentPermit.config.Config;
import nwu.studentPermit.events.ShowNotificationEvent;
import nwu.studentPermit.service.ConfigurationProxyService;
import nwu.studentPermit.service.ConfigurationProxyServiceCRUD;
import nwu.studentPermit.service.PermissionProxyService;
import nwu.studentPermit.service.PersonProxyService;
import nwu.studentPermit.service.TypeProxyService;
import nwu.studentPermit.ui.designs.MainViewDesign;
import nwu.studentPermit.util.QuestionOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@SpringComponent
@UIScope
@Translate
public class StudentPermitMainView extends MainViewDesign {

	private final Logger log = LoggerFactory.getLogger(StudentPermitMainView.class.getName());
	private PersonBiographicInfo personBiographic;

	@Autowired
	private ApplicationConfig applicationConfig;
	@Autowired
	private PersonProxyService personProxyService;
	@Autowired
	private PermissionProxyService permissionProxyService;
	@Autowired
	private EventBus eventBus;
	@Autowired
	private VaadinI18n vaadinI18n;
	@Autowired
	private Config config;
	@Autowired
	private ConfigurationProxyService configProxyService;
	@Autowired
	private ConfigurationProxyServiceCRUD configProxyServiceCRUD;
	@Autowired
	private TypeProxyService typeProxyService;

	private SessionContext sessionContext;
	private Button submitBtn;
	private ComboBox<QuestionOptions> questionnaireComboBox;
	private List<QuestionnaireInfo> questionnaireList;
	private FormLayout questionnaire;
	private FormLayout questionnaireLayout;

	public void initTheView() {
		setupUILayout();
		setSizeFull();
	}

	private void setupUILayout() {
		setupUniversityRow();
			setupUIComponents();
			setupButtons();
			setupEvents();
	}

	@Translate
	private void setupUIComponents() {
		questionnaire = new FormLayout();
		setQuestionnaires();
		appHeaderSection.addComponent(questionnaire);
	}

	@Translate
	private void setupButtons() {
		submitBtn = new Button(vaadinI18n.getMessage("ui.button.submit"));
		submitBtn.setStyleName(ValoTheme.BUTTON_PRIMARY);
		submitBtn.setVisible(false);
		appHeaderSection.addComponents(submitBtn);
	}

	private void setupEvents() {
		submitBtn.addClickListener((Button.ClickListener) clickEvent -> {
			StudentPermitCrudUI.getCurrent().clearErrors();
			if (validateUserInput()) {
				if (save()) {
					showConfirmationPopup();
				}
			}
		});

		questionnaireComboBox.addValueChangeListener(e -> {
			if(questionnaireLayout != null) {
				questionnaire.removeComponent(questionnaireLayout);
			}

			if(e.getValue() != null) {
				int selectedIndex = e.getValue().getOptionIndex();
				questionnaireComboBox.getValue().setSelectedIndex(selectedIndex);

				String selectedText = e.getValue().getOptionText();
				questionnaireComboBox.getValue().setSelectedText(selectedText);

				String selectedValue = e.getValue().getOptionValue();
				questionnaireComboBox.getValue().setSelectedValue(selectedValue);

				questionnaireLayout = generateQuestionnaire(questionnaireList.get(selectedIndex));
//				questionnaire.addComponent(questionnaireLayout);
				appHeaderSection.addComponent(questionnaireLayout, appHeaderSection.getComponentCount()-1);

				submitBtn.setVisible(true);
			} else {
				appHeaderSection.removeComponent(questionnaireLayout);
				submitBtn.setVisible(false);
			}
			questionnaireComboBox.setComponentError(null);
			StudentPermitCrudUI.getCurrent().clearErrors();
		});
	}

	private void setQuestionnaires(){
		questionnaireList = configProxyService.getQuestionnairesByApplication();
		questionnaireComboBox = new ComboBox<>(vaadinI18n.getMessage("ui.label.list.of.questionnaires"));
		List<QuestionOptions> options = new ArrayList<>();

		int index=0;
		boolean validOptionsToChoose = false;
		for (QuestionnaireInfo questionnaireInfo : questionnaireList) {
			boolean validOption = validateRole(questionnaireInfo.getIsForEmployees(), questionnaireInfo.getIsForStudents(),
					questionnaireInfo.getIsForAllNWU());

			if(!validOption) {
				continue;
			}
			validOptionsToChoose = true;
			String questionnaireID = questionnaireInfo.getLegacyKeys().get(questionnaireInfo.LEGACY_KEY_QUESTIONNAIRE_KQUESTIONNAIREID);
			String translatedTitle = vaadinI18n.getMessage(questionnaireInfo.getTitle());
			options.add(new QuestionOptions(questionnaireID, null, index++,
					translatedTitle, questionnaireInfo.getPurposeTypeKey()));
		}

		if(!validOptionsToChoose) {
			throw new VaadinUIException("validation.user.unauthorized", true);
		}

		questionnaireComboBox.setItems(options);
		questionnaireComboBox.setItemCaptionGenerator(QuestionOptions::getOptionText);
		questionnaireComboBox.setRequiredIndicatorVisible(true);
		questionnaireComboBox.setEmptySelectionAllowed(true);
		questionnaireComboBox.setTextInputAllowed(true);
		questionnaireComboBox.setPageLength(10);

		questionnaire.addComponent(questionnaireComboBox);
	}

	private FormLayout generateQuestionnaire(QuestionnaireInfo questionnaireInfo) {
		FormLayout questionnaireLayout = new FormLayout();
//		questionnaireLayout.setCaption(vaadinI18n.getMessage(questionnaireInfo.getTitle()));
		List<QuestionInfo> questions = questionnaireInfo.getQuestions();
		for (QuestionInfo question : questions) {
			addCheckBox(question, questionnaireLayout);
			addShortTextBox(question, questionnaireLayout);
			addLongTextBox(question, questionnaireLayout);
			addDropDown(question, questionnaireLayout);
			addRadioBox(question, questionnaireLayout);
		}

	return questionnaireLayout;
	}

	private void addRadioBox(QuestionInfo question, FormLayout questionnaire) {
		if (question.getIsCheckBox() != null && question.getIsCheckBox()) {
			List<QuestionOptions> options = getQuestionOptions(question);

			RadioButtonGroup<QuestionOptions> radioBox = new RadioButtonGroup<>(vaadinI18n.getMessage(question.getMessageKey()));
			radioBox.setItems(options);
			radioBox.setItemCaptionGenerator(QuestionOptions::getOptionText);
			radioBox.setStyleName("horizontal");
			radioBox.setRequiredIndicatorVisible(question.getIsMandatory());
			questionnaire.addComponent(radioBox);

			radioBox.addValueChangeListener(e -> {
				if (e.getValue() != null) {
					int selectedIndex = e.getValue().getOptionIndex();
					radioBox.getValue().setSelectedIndex(selectedIndex);

					String selectedText = e.getValue().getOptionText();
					radioBox.getValue().setSelectedText(selectedText);

					String selectedValue = e.getValue().getOptionValue();
					radioBox.getValue().setSelectedValue(selectedValue);
				}
				radioBox.setComponentError(null);
				StudentPermitCrudUI.getCurrent().clearErrors();
			});
		}
	}

	private List<QuestionOptions> getQuestionOptions(QuestionInfo question) {
		List<TypeInfo> values = typeProxyService.getTypesByCategory(question.getValueTypeKey(), sessionContext.getLang());
		List<QuestionOptions> options = new ArrayList<>();

		String questionnaireID = question.getLegacyKeys().get(question.LEGACY_KEY_QUESTIONNAIRE_FQUESTIONNAIREID);
		String questionID = question.getLegacyKeys().get(question.LEGACY_KEY_QUESTION_KQUESTIONID);
		int index = 0;
		for (TypeInfo typeInfo : values) {
			sessionContext.getLang();
			List<TypeNameInfo> names = typeInfo.getTypeNames();
			options.add(new QuestionOptions(questionnaireID, questionID, index++,
					getTypeKeyDescriptionBasedOnLang(names), typeInfo.getTypeKey()));
		}
		return options;
	}

	private String getTypeKeyDescriptionBasedOnLang(List<TypeNameInfo> names) {
		for (TypeNameInfo typeName : names) {
			if (sessionContext.getLang().toLowerCase().equals("vss.code.language.2")) {
				if (typeName.getLocale().toLowerCase().contains("af")) {
					return typeName.getShortDescr();
				}
			} else {	// English
				if (typeName.getLocale().toLowerCase().contains("en")) {
					return typeName.getShortDescr();
				}
			}
		}
		return "";
	}

	private QuestionOptions getQuestionOption(QuestionInfo question) {
		if(Strings.isNullOrEmpty(question.getValueTypeKey())) {
			log.error("This should never happen. Validation will be done serverside." +
					"There are no typekeys for the following message key returned from the server: " + question.getMessageKey());
			return null;
		}

		List<QuestionOptions> options = getQuestionOptions(question);

		if(options.size() < 1) {
			return null;
		}

		return options.get(0);
	}

	private void addDropDown(QuestionInfo question, FormLayout questionnaire) {
		if (question.getIsDropDown() != null && question.getIsDropDown()) {
			ComboBox<QuestionOptions> question2Combobox = new ComboBox<>(vaadinI18n.getMessage(question.getMessageKey()));

			List<QuestionOptions> options = getQuestionOptions(question);
			question2Combobox.setItems(options);
			question2Combobox.setRequiredIndicatorVisible(question.getIsMandatory());
			question2Combobox.setEmptySelectionAllowed(true);
			question2Combobox.setItemCaptionGenerator((ItemCaptionGenerator<QuestionOptions>) QuestionOptions::getOptionText);

			question2Combobox.addValueChangeListener(e -> {
				question2Combobox.setComponentError(null);
				StudentPermitCrudUI.getCurrent().clearErrors();

				if (e.getValue() == null) {
					return;
				}

				int selectedIndex = e.getValue().getOptionIndex();
				question2Combobox.getValue().setSelectedIndex(selectedIndex);

				String selectedText = e.getValue().getOptionText();
				question2Combobox.getValue().setSelectedText(selectedText);

				QuestionOptions selectedValue = e.getValue();
				question2Combobox.setValue(selectedValue);
			});
			questionnaire.addComponent(question2Combobox);
		}
	}

	private void addShortTextBox(QuestionInfo question, FormLayout questionnaire) {
		if (question.getIsShortAnswer() != null && question.getIsShortAnswer()) {
			TextField shortTextField = new TextField(vaadinI18n.getMessage(question.getMessageKey()));
			shortTextField.setRequiredIndicatorVisible(question.getIsMandatory());
			shortTextField.setMaxLength(20);
			shortTextField.setData(question);
			questionnaire.addComponent(shortTextField);

			shortTextField.addValueChangeListener(e -> {
				shortTextField.setComponentError(null);
				StudentPermitCrudUI.getCurrent().clearErrors();
			});
		}
	}
	
	private void addLongTextBox(QuestionInfo question, FormLayout questionnaire) {
		if (question.getIsLongAnswer() != null && question.getIsLongAnswer()) {
			TextField longTextField = new TextField(vaadinI18n.getMessage(question.getMessageKey()));
			longTextField.setRequiredIndicatorVisible(question.getIsMandatory());
			longTextField.setMaxLength(2000);
			longTextField.setData(question);
			questionnaire.addComponent(longTextField);

			longTextField.addValueChangeListener(e -> {
				longTextField.setComponentError(null);
				StudentPermitCrudUI.getCurrent().clearErrors();
			});
		}
	}

	private void addCheckBox(QuestionInfo question, FormLayout questionnaire) {
		if (question.getIsMultipleChoice() != null && question.getIsMultipleChoice()) {
			HorizontalLayout hlayout = new HorizontalLayout();
			hlayout.setCaptionAsHtml(true);
			hlayout.setCaption(vaadinI18n.getMessage(question.getMessageKey()));
			for (int i = 1; i < 4; i++) {
				CheckBox question3 = new CheckBox("Option " + i);
				question3.setRequiredIndicatorVisible(question.getIsMandatory());
				question3.setData(question);
				hlayout.addComponent(question3);

				question3.addValueChangeListener(e -> {
					question3.setComponentError(null);
					StudentPermitCrudUI.getCurrent().clearErrors();
				});
			}
			questionnaire.addComponent(hlayout);
		}
	}

	@Translate
	private Boolean validateUserInput() {
		if(questionnaireLayout != null) {
			for (int i = 0; i < questionnaireLayout.getComponentCount(); i++) {
				Component question = questionnaireLayout.getComponent(i);

				if (question instanceof RadioButtonGroup) {
					RadioButtonGroup rbg = ((RadioButtonGroup) question);
					if (rbg.isRequiredIndicatorVisible() && rbg.getValue() == null) {
						rbg.setComponentError(new UserError(vaadinI18n.getMessage("validation.radiobutton.mandatory")));
						throw new VaadinUIException(vaadinI18n.getMessage("user.feedback.submit.unsuccessful"));
					}
				}

				if (question instanceof TextField) {
					TextField tf = ((TextField) question);
					if(tf.isRequiredIndicatorVisible() && tf.getValue() == (null)) {
						tf.setComponentError(new UserError(vaadinI18n.getMessage("validation.textfield.mandatory")));
						throw new VaadinUIException(vaadinI18n.getMessage("user.feedback.submit.unsuccessful"));
					}
				}

				if (question instanceof CheckBox) {
					CheckBox cb = ((CheckBox) question);
					if(cb.isRequiredIndicatorVisible() && cb.getValue() == (null)) {
						cb.setComponentError(new UserError(vaadinI18n.getMessage("validation.checkbox.mandatory")));
						throw new VaadinUIException(vaadinI18n.getMessage("user.feedback.submit.unsuccessful"));
					}
				}

				if (question instanceof HorizontalLayout) {
					for (Component option : (HorizontalLayout) question) {
						if (option instanceof CheckBox) {
							CheckBox cb = ((CheckBox) option);
							if(cb.isRequiredIndicatorVisible() && cb.getValue() == (null)) {
								cb.setComponentError(new UserError(vaadinI18n.getMessage("validation.checkbox.mandatory")));
								throw new VaadinUIException(vaadinI18n.getMessage("user.feedback.submit.unsuccessful"));
							}
						}
					}
				}

				if (question instanceof ComboBox) {
					ComboBox cbb = ((ComboBox) question);
					if(cbb.isRequiredIndicatorVisible() && cbb.getValue() == (null)) {
						cbb.setComponentError(new UserError(vaadinI18n.getMessage("validation.droplist.mandatory")));
						throw new VaadinUIException(vaadinI18n.getMessage("user.feedback.submit.unsuccessful"));
					}
				}
			}
		}
		return true;
	}

	@Translate
	private Boolean save() {

		if (questionnaireLayout != null) {

			for (int i = 0; i < questionnaireLayout.getComponentCount(); i++) {
				Component question = questionnaireLayout.getComponent(i);
				BusinessEntityQuestionAnswerInfo businessEntityQuestionAnswerInfo = getQuestionsAndAnswers(question);;
				boolean createdQuestionAnswer = configProxyServiceCRUD.createQuestionAnswer(businessEntityQuestionAnswerInfo);

				if(!createdQuestionAnswer) {
					return false;
				}
			}
			eventBus.post(new ShowNotificationEvent(vaadinI18n.getMessage("user.feedback.submit.successful"), SeverityType.SUCCESS));
			return true;
		} else {
			return false;
		}
	}

	private BusinessEntityQuestionAnswerInfo getQuestionsAndAnswers(Component question) {
		BusinessEntityQuestionAnswerInfo businessEntityQuestionAnswerInfo = new BusinessEntityQuestionAnswerInfo();
		businessEntityQuestionAnswerInfo.setActualQuestionText(question.getCaption());
		businessEntityQuestionAnswerInfo.setDateAnswered(new Date());

		MetaInfo metaInfo = new MetaInfo();
		metaInfo.setCreateId(sessionContext.getAuthenticatedUser());
		metaInfo.setAuditFunction("999");
		businessEntityQuestionAnswerInfo.setMetaInfo(metaInfo);

		HashMap<String, String> legacyKeys = new HashMap<>();
		legacyKeys.put(BusinessEntityQuestionAnswerInfo.LEGACY_KEY_BUSINESSENTITY_FBUSINESSENTITYID, sessionContext.getAuthenticatedUser());

		businessEntityQuestionAnswerInfo.setLegacyKeys(legacyKeys);

		//AbstractSingleSelect covers Radio button and Combobox
		if (question instanceof AbstractSingleSelect<?>) {
			AbstractSingleSelect<?> rbg = ((AbstractSingleSelect<?>) question);
			if (rbg.getValue() != null) {

				businessEntityQuestionAnswerInfo.setAnswerTypeKey(((QuestionOptions) rbg.getValue()).getSelectedValue());
				legacyKeys.put(BusinessEntityQuestionAnswerInfo.LEGACY_KEY_QUESTION_KQUESTIONID, ((QuestionOptions) rbg.getValue()).getQuestionID());
				legacyKeys.put(BusinessEntityQuestionAnswerInfo.LEGACY_KEY_QUESTIONNAIRE_FQUESTIONNAIREID, ((QuestionOptions) rbg.getValue()).getQuestionnaireID());
			}
		}

		if(question instanceof AbstractTextField) {
			AbstractTextField comboBoxQuestion = ((AbstractTextField) question);
			if (comboBoxQuestion.getValue() != null) {

				legacyKeys.put(BusinessEntityQuestionAnswerInfo.LEGACY_KEY_QUESTION_KQUESTIONID,
						((QuestionInfo) comboBoxQuestion.getData()).getLegacyKeys().get(QuestionInfo.LEGACY_KEY_QUESTION_KQUESTIONID));

				legacyKeys.put(BusinessEntityQuestionAnswerInfo.LEGACY_KEY_QUESTIONNAIRE_FQUESTIONNAIREID,
						((QuestionInfo) comboBoxQuestion.getData()).getLegacyKeys()
								.get(QuestionInfo.LEGACY_KEY_QUESTIONNAIRE_FQUESTIONNAIREID));
			}
		}

		handleRadioButton(question, legacyKeys);
		return businessEntityQuestionAnswerInfo;
	}

	private void handleRadioButton(Component question, HashMap<String, String> legacyKeys ) {
		Boolean horizontalLayout = question instanceof HorizontalLayout;

		if(!horizontalLayout) {
			return;
		}

		if(((HorizontalLayout) question).getComponentCount() > 10 && ((HorizontalLayout) question).getComponentCount() < 1) {
			return;
		}

		for(int i = 0; i < ((HorizontalLayout) question).getComponentCount(); i++) {
			CheckBox checkBox = (CheckBox) ((HorizontalLayout) question).getComponent(i);

			if(!checkBox.getValue()) {
				continue;
			}

			legacyKeys.put(BusinessEntityQuestionAnswerInfo.LEGACY_KEY_QUESTION_KQUESTIONID,
					((QuestionInfo) checkBox.getData()).getLegacyKeys().get(QuestionInfo.LEGACY_KEY_QUESTION_KQUESTIONID));

			legacyKeys.put(BusinessEntityQuestionAnswerInfo.LEGACY_KEY_QUESTIONNAIRE_FQUESTIONNAIREID,
					((QuestionInfo) checkBox.getData()).getLegacyKeys()
							.get(QuestionInfo.LEGACY_KEY_QUESTIONNAIRE_FQUESTIONNAIREID));
			return;

		}
	}

	@Translate
	private void showConfirmationPopup() {
		Label popupMessage = new Label(vaadinI18n.getMessage("submit.save.success.popup.message"));
		popupMessage.setContentMode(ContentMode.HTML);
		popupMessage.setSizeFull();
		Button button = new Button(vaadinI18n.getMessage("submit.save.success.popup.button.ok"));
		Window window = new Window(vaadinI18n.getMessage("submit.save.success.popup.title"));
		window.setResizable(false);
		window.center();
		window.setWidth(300.0f, Unit.PIXELS);
		VerticalLayout popupContent = new VerticalLayout();
		popupContent.setMargin(true);
		popupContent.addComponents(popupMessage, button);
		popupContent.setComponentAlignment(button, Alignment.MIDDLE_RIGHT);
		window.setContent(popupContent);
		UI.getCurrent().addWindow(window);

		button.addClickListener(new Button.ClickListener() {
			@Override
			public void buttonClick(Button.ClickEvent clickEvent) {
				window.close();
				navigateToDIY();
			}
		});

		window.addCloseListener(new Window.CloseListener() {
			@Override
			public void windowClose(Window.CloseEvent closeEvent) {
				window.close();
				navigateToDIY();
			}
		});
	}

	private void navigateToDIY() {
		if (UI.getCurrent().getLocale().getLanguage().equals("af")) {
			getUI().getPage().setLocation(applicationConfig.getDIY_UrlAF());
		} else {
			getUI().getPage().setLocation(applicationConfig.getDIY_UrlEN());
		}
	}

	public void setupPersonBioInformation() {
		try {
			personBiographic = personProxyService.getPersonBiographic(sessionContext.getLookupUser(),
					sessionContext.getLang());
		} catch (VaadinUIException vaadinEx) {
			throw vaadinEx;
		} catch (Exception e) {
			throw new VaadinUIException("Cannot connect to person proxy service: " + e, false);
		}

		boolean isAdminUser = applicationConfig.getCaseEnabled().equalsIgnoreCase("true") ? false : true;

		getUniversityNumber().setVisible(!isAdminUser);
		universityNumberTextField.setVisible(isAdminUser);

		this.getUniversityNumber().setValue(sessionContext.getLookupUser());
		universityNumberTextField.setValue(sessionContext.getLookupUser());
		universityNumberTextField.setValueChangeMode(ValueChangeMode.BLUR);

		universityNumberTextField.addValueChangeListener((HasValue.ValueChangeListener<String>) event -> {
			universityNumberTextField.setComponentError(null);
			sessionContext.setLookupUser(universityNumberTextField.getValue());
			sessionContext.setAuthenticatedUser(universityNumberTextField.getValue());
			setupPersonBioInformation();
			clearUserData();
			loadUserData();
		});
		String[] personTitleTypeKey = personBiographic.getTitleTypeKey().split("\\.");

		this.getNameOfAccountHolder().setValue(
				personTitleTypeKey[4] + " " + personBiographic.getInitials() + " " + personBiographic.getLastName());
	}

	public void setSessionContext(SessionContext sessionContext) {
		this.sessionContext = sessionContext;
		setupPersonBioInformation();
	}

	public Boolean validateRole(Boolean isForEmployee, Boolean isForStudent, Boolean isForAllNWU) {
		String validRole = null;
		Boolean isValidRole = false;

		if(isForEmployee) {
			validRole = "vss.code.HOEDANIG.W";
		} else if (isForStudent){
			validRole = "vss.code.HOEDANIG.S";
		} else if (isForAllNWU){
			isValidRole = true;
		}

		if(!isForAllNWU) {
			List<PersonAffiliationInfo> personAffiliations = personProxyService
					.getPersonAffiliation(sessionContext.getLookupUser());

			if (personAffiliations != null) {
				for (PersonAffiliationInfo personAffiliationInfo : personAffiliations) {
					
					if (personAffiliationInfo.getAffiliationTypeKey().contains(validRole)) {
						isValidRole = true;
						break;
					}
				}
			}
		}

		return isValidRole;
	}

	public void clearUserData() {
		for (int i = 0; i < questionnaire.getComponentCount(); i++) {
			Component question = questionnaire.getComponent(i);
			if (question instanceof RadioButtonGroup) {
				((RadioButtonGroup) question).setValue(null);
			}

			if (question instanceof TextField) {
				((TextField) question).setValue("");
			}

			if (question instanceof CheckBox) {
				((CheckBox) question).setValue(false);
			}

			if (question instanceof HorizontalLayout) {
				for (Component option : (HorizontalLayout) question) {
					if (option instanceof CheckBox) {
						((CheckBox) option).setValue(false);
					}
				}
			}

			if (question instanceof ComboBox) {
				((ComboBox) question).setValue(null);
			}
		}
		StudentPermitCrudUI.getCurrent().clearErrors();
	}

	public void loadUserData() {
		// TODO Method to load questionnaire data from DB via service for edit/view purposes if it exist.
	}

	public void setAppName(Label appName) {
		this.appName = appName;
	}

	@Translate("ui.label.university.number")
	public Label getUniversityNumberHeading() {
		return universityNumberHeading;
	}

	public Label getUniversityNumber() {
		return universityNumber;
	}

	public Label getNameOfAccountHolder() {
		return this.nameOfAccountHolder;
	}

	@Translate("ui.app.title")
	public Label getAppName() {
		return appName;
	}

	private void setupUniversityRow() {
		appHeaderSection.setWidth(435.0f, Unit.PIXELS);
		setComponentAlignment(appHeaderSection, Alignment.TOP_LEFT);
	}

	public String getCaption() {
		return null;
	}

	public ApplicationConfig getApplicationConfig() {
		return applicationConfig;
	}

}
