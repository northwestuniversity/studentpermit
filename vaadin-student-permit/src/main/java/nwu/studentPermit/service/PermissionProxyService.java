package nwu.studentPermit.service;

import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.framework.auth.KeyStoreManager;
import nwu.ac.za.framework.i18n.VaadinI18n;
import nwu.ac.za.framework.service.AbstractServiceProxy;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.enabling.capabilities.supporting.service.permission.service.PermissionService;
import nwu.enabling.capabilities.supporting.service.permission.service.factory.PermissionServiceClientFactory;
import nwu.studentPermit.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

@Service
@RefreshScope
public class PermissionProxyService extends AbstractServiceProxy<PermissionService> {

    @Autowired
    private Config config;

    @Autowired
    private VaadinI18n vaadinI18n;

    @Autowired
    private KeyStoreManager keyStoreManager;

    @Override
    protected PermissionService initService() throws Exception {

        String wsMajorVersion = ServiceRegistryLookupUtility.getMajorVersion(config.getPermissionServiceApiVersion());
        String appRuntimeEnv = config.getRuntimeEnvironment();

        String permissionServiceLookupKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(
                PermissionServiceClientFactory.PERMISSIONSERVICE,
                wsMajorVersion,
                config.getWebServiceDatabase(),
                appRuntimeEnv);

        log.info("LookupKey: " + permissionServiceLookupKey + "\n");

        try {
            return PermissionServiceClientFactory.getPermissionService(
                    permissionServiceLookupKey,
                    config.getPermissionServiceReadUsername(),
                    keyStoreManager.decrypt(config.getPermissionServiceReadPassword()));

        } catch (Exception ex) {
            log.error("Could not initialize Permission service: " + ex);
            throw new VaadinUIException("Could not initialize Permission service.");
        }
    }
}

