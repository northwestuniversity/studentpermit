package nwu.studentPermit.service;

import assemble.edu.exceptions.DoesNotExistException;
import assemble.edu.exceptions.InvalidParameterException;
import assemble.edu.exceptions.MissingParameterException;
import assemble.edu.exceptions.OperationFailedException;
import assemble.edu.exceptions.PermissionDeniedException;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.framework.auth.KeyStoreManager;
import nwu.ac.za.framework.service.AbstractServiceProxy;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.enabling.capabilities.supporting.service.configuration.service.ConfigurationService;
import nwu.enabling.capabilities.supporting.service.configuration.service.factory.ConfigurationServiceClientFactory;
import nwu.enabling.capabilities.supporting.service.question.dto.QuestionnaireInfo;
import nwu.studentPermit.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RefreshScope
public class ConfigurationProxyService extends AbstractServiceProxy<ConfigurationService> {
    
    @Autowired
    private Config config;

    @Autowired
    private KeyStoreManager keyStoreManager;

    @Override
    protected ConfigurationService initService() throws Exception {

        String wsMajorVersion = ServiceRegistryLookupUtility.getMajorVersion(config.getConfigurationManagmentApiVersion());
        String appRuntimeEnv = config.getRuntimeEnvironment();
        String lookupKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(
                ConfigurationServiceClientFactory.CONFIGURATIONSERVICE, wsMajorVersion, config.getWebServiceDatabase(),
                appRuntimeEnv);

        try {
            String decryptedPassword = keyStoreManager.decrypt(config.getConfigurationManagementReadPassword());
            return ConfigurationServiceClientFactory.getConfigurationService(lookupKey,
                    config.getConfigurationManagementReadUsername(), decryptedPassword);
        } catch (Exception ex) {
            log.error("Could not initialize Config service: " + lookupKey + "\n Error:" + ex);
            throw new VaadinUIException("error.configservice.connection",true);
        }
    }

    public HashMap<String, String> getAllFacultyApprovers() {
        String appRuntimeEnv = config.getRuntimeEnvironment();
        String directoryKey = appRuntimeEnv + "/" + "ADDS-AND-DROPS/FACULTYAPPROVERS";
        return setupMapFromDirectory(directoryKey);

    }

    public HashMap<String, String> getAllCampusQualificationCurriculumsToExclude() {
        String appRuntimeEnv = config.getRuntimeEnvironment();
        String directoryKey = appRuntimeEnv + "/" + "ADDS-AND-DROPS/EXCLUDE.CAMPUS.QUALIFICATION.CURRICULUM";
        return setupMapFromDirectory(directoryKey);

    }

    public HashMap<String, String> getAllFacultyCampusApprovers() {
        String appRuntimeEnv = config.getRuntimeEnvironment();
        String directoryKey = appRuntimeEnv + "/" + "ADDS-AND-DROPS/FACULTY.CAMPUS.APPROVERS";
        return setupMapFromDirectory(directoryKey);

    }

    private HashMap<String, String> setupMapFromDirectory(String directoryKey) {
        try {
            HashMap<String, String> result = getService().getConfigurationByDirectory(directoryKey , getContextInfo(null));
            HashMap<String, String> facultHashMap = new HashMap<String, String>();

            // Strip the folder infront of the faculty
            for (String facultyKey : result.keySet()) {
                String value = result.get(facultyKey);
                int lastIndex = facultyKey.lastIndexOf("/");
                String actualFaculty = facultyKey.substring(lastIndex+1);
                facultHashMap.put(actualFaculty, value);
            }
            return facultHashMap;
        } catch (DoesNotExistException e) {
            log.error("Could not setup from directory on the ConfigService" + directoryKey);
            throw new VaadinUIException("error.faculty.campus.approvers.doesnotexist",true);

        } catch (InvalidParameterException | MissingParameterException
                | OperationFailedException e) {
            log.error("Could not setup from directory on the ConfigService: " + directoryKey);
            throw new VaadinUIException("error.facultyapprovers.operationfailed",true);
        } catch (PermissionDeniedException e) {
            log.error("Could not setup from directory on the ConfigService: " + directoryKey);
            throw new VaadinUIException("error.facultyapprovers.permissiondenied",true);
        }
    }

    public Map<String, String> getApllicationVersionSpecificConfiguration() {
        try {

            log.info("Calling ConfigurationService method getConfigurationByApplication: \n" +
                    "Environment: " + config.getRuntimeEnvironment() + ".\n" +
                    "config.getAppName(): " + config.getAppName() + ".\n" +
                    "config.getAppMajorVersion(): " + config.getAppMajorVersion());

            HashMap<String, String> configValues = this.getService().getConfigurationByApplication(
                    config.getRuntimeEnvironment(), config.getAppName(), "V".concat(config.getAppMajorVersion()), getContextInfo(null));
            return configValues;
        } catch (DoesNotExistException ex) {
            log.warn("There are no Configuration for the application: " + ex.getMessage(), ex);
            return null;
        } catch (MissingParameterException | OperationFailedException | InvalidParameterException var4) {
            String exp = "Reading of Application Configuration Failed";
            throw new RuntimeException(exp, var4);
        } catch (PermissionDeniedException var5) {
            String exp = "Permission denied: Reading of Application Configuration Failed.";
            throw new RuntimeException(exp, var5);
        }
    }

    public Map<String, String> getApplicationGlobalConfiguration() {
        String exp;
        try {
            return ((ConfigurationService)this.getService()).getConfigurationByApplication(config.getRuntimeEnvironment(),
                    config.getAppName(), "GLOBAL", getContextInfo(null));
        } catch (DoesNotExistException var3) {
            exp = "There are no Global Configuration for the application " + config.getAppName() + " " + config.getAppMajorVersion();
            throw new RuntimeException(exp, var3);
        } catch (MissingParameterException | OperationFailedException | InvalidParameterException var4) {
            exp = "Reading of Application Global Configuration Failed" + config.getAppName() + " " + config.getAppMajorVersion();
            throw new RuntimeException(exp, var4);
        } catch (PermissionDeniedException var5) {
            exp = "Permission denied: Reading of Application Global Configuration Failed" + config.getAppName() + " " + config.getAppMajorVersion();
            throw new RuntimeException(exp, var5);
        }
    }

    public List<QuestionnaireInfo> getQuestionnairesByApplication() {
        
        try {
            List<QuestionnaireInfo> results = ((ConfigurationService)this.getService()).getQuestionnaireByApplication(
                    config.getAppName().toUpperCase(), getContextInfo(null));
            return results;
        } catch (DoesNotExistException var3) {
            throw new VaadinUIException(var3.getMessage(), false, false);
        } catch (MissingParameterException | OperationFailedException | InvalidParameterException var4) {
        	throw new VaadinUIException(var4.getMessage(), false, false);
        } catch (PermissionDeniedException var5) {
        	throw new VaadinUIException(var5.getMessage(), false, false);
        }
    }

}

