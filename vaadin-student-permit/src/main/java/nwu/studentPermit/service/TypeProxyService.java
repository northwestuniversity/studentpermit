package nwu.studentPermit.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import ac.za.nwu.core.type.dto.TypeInfo;
import ac.za.nwu.core.type.service.TypeService;
import ac.za.nwu.core.type.service.factory.TypeServiceClientFactory;
import assemble.edu.common.dto.ContextInfo;
import assemble.edu.exceptions.DoesNotExistException;
import assemble.edu.exceptions.InvalidParameterException;
import assemble.edu.exceptions.MissingParameterException;
import assemble.edu.exceptions.OperationFailedException;
import assemble.edu.exceptions.PermissionDeniedException;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.framework.auth.KeyStoreManager;
import nwu.ac.za.framework.service.AbstractServiceProxy;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.studentPermit.config.Config;

@Service
@RefreshScope
public class TypeProxyService extends AbstractServiceProxy<TypeService> {

    @Autowired
    private Config config;

    @Autowired
    private KeyStoreManager keyStoreManager;

    @Override
    protected TypeService initService() throws Exception {
        String wsMajorVersion = ServiceRegistryLookupUtility.getMajorVersion(config.getiAPIVersion());
        String appRuntimeEnv = config.getRuntimeEnvironment();
        String typeServiceKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(
                TypeServiceClientFactory.TYPESERVICE,
                wsMajorVersion,
                config.getWebServiceDatabase(),
                appRuntimeEnv);

        try {
            return TypeServiceClientFactory.getTypeService(typeServiceKey, config.getiAPIUsername(),
                    keyStoreManager.decrypt(config.getiAPIPassword()));
        } catch (Exception ex) {
            log.error("Could not connect to TypeService: " + ex);
            throw new VaadinUIException("error.typeservice.fail", true);
        }

    }

    public List<TypeInfo> getTypesByCategory(String category, String language) {
        try {
            return getService().getTypesByCategory(category, language, getContextInfo(null));

        } catch (DoesNotExistException e) {
            log.error("Could not find Types By category: " + e.getMessage(), e);
            throw new VaadinUIException("error.typeservice.not.found", true);

        } catch (InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException e) {
            log.error("Exception occurred while getting Types By category: " + e.getMessage(), e);
            throw new VaadinUIException("error.typeservice.error", true);

        } catch (Exception e) {
            log.error("Unexpected Exception occurred while getting Types By category: " + e.getMessage(), e);
            throw new VaadinUIException("error.typeservice.failed", true);
        }
    }

}
