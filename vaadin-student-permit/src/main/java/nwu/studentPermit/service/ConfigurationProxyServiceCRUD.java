package nwu.studentPermit.service;

import assemble.edu.common.dto.ContextInfo;
import assemble.edu.exceptions.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.framework.auth.KeyStoreManager;
import nwu.ac.za.framework.i18n.VaadinI18n;
import nwu.ac.za.framework.service.AbstractServiceProxy;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.enabling.capabilities.supporting.service.configuration.service.crud.ConfigurationServiceCRUD;
import nwu.enabling.capabilities.supporting.service.configuration.service.crud.factory.ConfigurationServiceCRUDClientFactory;
import nwu.enabling.capabilities.supporting.service.question.dto.BusinessEntityQuestionAnswerInfo;
import nwu.studentPermit.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@RefreshScope
public class ConfigurationProxyServiceCRUD extends AbstractServiceProxy<ConfigurationServiceCRUD> {
    
    @Autowired
    private Config config;

    @Autowired
    private KeyStoreManager keyStoreManager;

    @Autowired
    private VaadinI18n vaadinI18n;

    @Override
    protected ConfigurationServiceCRUD initService() throws Exception {

        String wsMajorVersion = ServiceRegistryLookupUtility.getMajorVersion(config.getConfigurationManagmentApiVersion());
        String appRuntimeEnv = config.getRuntimeEnvironment();
        String lookupKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(
                ConfigurationServiceCRUDClientFactory.CONFIGURATIONSERVICECRUD, wsMajorVersion, config.getWebServiceDatabase(),
                appRuntimeEnv);

        try {
            String decryptedPassword = keyStoreManager.decrypt(config.getConfigurationManagementWritePassword());
            return ConfigurationServiceCRUDClientFactory.getConfigurationServiceCRUD(lookupKey,
                    config.getConfigurationManagementWriteUsername(), decryptedPassword);
        } catch (Exception ex) {
            log.error("Could not initialize Config service: " + lookupKey + "\n Error:" + ex);
            throw new VaadinUIException("error.configservice.connection",true);
        }
    }

    public boolean createQuestionAnswer(BusinessEntityQuestionAnswerInfo businessEntityQuestionAnswerInfo) {
        ObjectWriter writer = new ObjectMapper().writerWithDefaultPrettyPrinter();
        try {
            log.info("Request to ConfigurationServiceCRUD method createQuestionAnswer: " +
                    writer.writeValueAsString(businessEntityQuestionAnswerInfo));

        } catch (IOException e) {
            log.warn("Could not map request BusinessEntityQuestionAnswerInfo to string exception: " + e);
        }

        try {

            getService().createQuestionAnswer(businessEntityQuestionAnswerInfo, new ContextInfo(config.getAppName()));
            return true;
        } catch (DoesNotExistException doesNotExistException) {
            log.error("Could not create QuestionAnswer not found: " + doesNotExistException.getMessage());
            throw new VaadinUIException(vaadinI18n.getMessage("user.feedback.submit.unsuccessful") + " " +
                    doesNotExistException.getMessage());
        } catch (InvalidParameterException | MissingParameterException | OperationFailedException | PermissionDeniedException ex) {

            log.error("Could not create QuestionAnswer ref: 1001: " + ex.getMessage());
            throw new VaadinUIException(vaadinI18n.getMessage("user.feedback.submit.unsuccessful") + " " + ex.getMessage());

        } catch (Exception ex) {
            log.error("Could not create QuestionAnswer: ref: 1002: " + ex.getMessage());
            throw new VaadinUIException(vaadinI18n.getMessage("user.feedback.submit.unsuccessful") + " " + ex.getMessage());
        }
    }
}

