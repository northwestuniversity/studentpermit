package nwu.studentPermit.service;

import ac.za.nwu.core.person.dto.PersonAffiliationInfo;
import ac.za.nwu.core.person.dto.PersonBiographicInfo;
import ac.za.nwu.core.person.dto.PersonPreferenceInfo;
import ac.za.nwu.core.person.service.PersonService;
import ac.za.nwu.core.person.service.factory.PersonServiceClientFactory;
import assemble.edu.exceptions.DoesNotExistException;
import assemble.edu.exceptions.InvalidParameterException;
import assemble.edu.exceptions.MissingParameterException;
import assemble.edu.exceptions.OperationFailedException;
import assemble.edu.exceptions.PermissionDeniedException;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.framework.auth.KeyStoreManager;
import nwu.ac.za.framework.i18n.VaadinI18n;
import nwu.ac.za.framework.service.AbstractServiceProxy;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.studentPermit.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Henriko on 2020-04-29.
 */
@Service
@RefreshScope
public class PersonProxyService extends AbstractServiceProxy<PersonService> {

    private static final String PERSON_PREFERENCE_CORR_LANGUAGE = "vss.code.TAALDOEL.C";
    @Autowired
    private Config config;

    @Autowired
    private VaadinI18n vaadinI18n;

    @Autowired
    private KeyStoreManager keyStoreManager;

    @Override
    protected PersonService initService() throws Exception {

        String wsMajorVersion = ServiceRegistryLookupUtility.getMajorVersion(config.getiAPIVersion());
        String appRuntimeEnv = config.getRuntimeEnvironment();

        String personServiceLookupKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(
                PersonServiceClientFactory.PERSONSERVICE,
                wsMajorVersion,
                config.getWebServiceDatabase(),
                appRuntimeEnv);

        log.info("LookupKey: " + personServiceLookupKey + "\n");

        try {
            return PersonServiceClientFactory.getPersonService(
                    personServiceLookupKey,
                    config.getiAPIUsername(),
                    keyStoreManager.decrypt(config.getiAPIPassword()));

        } catch (Exception ex) {
            log.error("Could not initialize Person service: " + ex);
            throw new VaadinUIException("Could not initialize Person service.");
        }
    }

    public PersonBiographicInfo getPersonBiographicByLang(String lookupUser, String systemLanguag) {
        try {
            return getService().getPersonBiographicByLang(lookupUser, systemLanguag,getContextInfo(null));
        } catch (DoesNotExistException | InvalidParameterException | OperationFailedException | MissingParameterException | PermissionDeniedException e) {
            log.info("Could not retrieve person from person service: " + e);

            throw new RuntimeException(e);
        }
    }

    public PersonBiographicInfo getPersonBiographic(String lookupUser, String systemLanguageTypeKey) {
        try {
            return getService().getPersonBiographicByLang(lookupUser, systemLanguageTypeKey, getContextInfo(null));
        } catch (InvalidParameterException | OperationFailedException | MissingParameterException | PermissionDeniedException e) {
            log.error("Could not retrieve person from person service: " + e);

            throw new VaadinUIException("Could not get person biographic for user: " + lookupUser + ": " + e);
        } catch (DoesNotExistException e1) {
            log.error("error.person.doesnot.exist" + lookupUser);
            throw new VaadinUIException("error.person.doesnot.exist",true);
        } catch (Exception ex) {
            log.error("Could not get Person Biographic for user: " + lookupUser + " : " + ex);

            throw new VaadinUIException(vaadinI18n.getMessage("error.person.service.biographic") +
                    lookupUser + ".\n " +
                    vaadinI18n.getMessage("error.person.service.exception") + ex + "\n" +
                    vaadinI18n.getMessage("error.person.service.contact"));
        }
    }

    public String composeFullName(PersonBiographicInfo personBiographic) {
        if (personBiographic != null) {
            String[] personTitleTypeKey = personBiographic.getTitleTypeKey().split("\\.");
            return personTitleTypeKey[4] + " " + personBiographic.getInitials() + " "
                    + personBiographic.getLastName();

        }

        return "CANNOT BUILD NAME";
    }

    public List<PersonAffiliationInfo> getPersonAffiliation(String lookupUser) {
        try {
            return getService().getPersonAffiliation(lookupUser, getContextInfo(null));
        } catch (DoesNotExistException | InvalidParameterException | MissingParameterException | PermissionDeniedException | OperationFailedException e) {
            log.error(e.getMessage(), e);
            throw new VaadinUIException(e.getMessage());
        } catch (Exception ex) {
            log.error("Could not get Person Affiliation for user: " + lookupUser + " : " + ex);
            throw new VaadinUIException("Could not get Person Affiliation for user: " + lookupUser);
        }
    }

    public List<PersonPreferenceInfo> getPersonPreference(String universityNumber){
        try {
            return getService().getPersonPreference(universityNumber, getContextInfo(null));
        } catch (DoesNotExistException | InvalidParameterException | MissingParameterException | PermissionDeniedException | OperationFailedException e) {
            log.error(e.getMessage(), e);
            throw new VaadinUIException(e.getMessage());
        }
    }

    public PersonPreferenceInfo getPersonPreferredCorrLanguage(String universityNumber){
        try {
            PersonPreferenceInfo studentPreferenceInfo = getService().getPersonPreferenceByType(
                    universityNumber, PERSON_PREFERENCE_CORR_LANGUAGE, getContextInfo(null));
            return studentPreferenceInfo;
        } catch (DoesNotExistException | InvalidParameterException | MissingParameterException | PermissionDeniedException | OperationFailedException e) {
            log.error(e.getMessage(), e);
            throw new VaadinUIException(e.getMessage());
        }
    }
}

