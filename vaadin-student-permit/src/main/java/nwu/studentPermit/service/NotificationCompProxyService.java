package nwu.studentPermit.service;

import ac.za.nwu.notification.service.NotificationCompService;
import ac.za.nwu.notification.service.NotificationCompServiceNamespace;
import ac.za.nwu.registry.utility.GenericServiceClientFactory;
import nwu.ac.za.framework.ServiceRegistryLookupUtility;
import nwu.ac.za.framework.auth.KeyStoreManager;
import nwu.ac.za.framework.i18n.VaadinI18n;
import nwu.ac.za.framework.service.AbstractServiceProxy;
import nwu.ac.za.ui.utils.exceptions.VaadinUIException;
import nwu.studentPermit.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;

@Service
@RefreshScope
public class NotificationCompProxyService extends AbstractServiceProxy<NotificationCompService> {

    @Autowired
    private Config config;

    @Autowired
    private VaadinI18n vaadinI18n;

    @Autowired
    private KeyStoreManager keyStoreManager;

    @Override
    protected NotificationCompService initService() throws Exception {

        String wsMajorVersion = ServiceRegistryLookupUtility.getMajorVersion(config.getNotificationCompAPIVersion());
        String appRuntimeEnv = config.getRuntimeEnvironment();

        String nofitificationCompServiceLookupKey = ServiceRegistryLookupUtility.getServiceRegistryLookupKey(
                NotificationCompServiceNamespace.NOTIFICATION_NOTIFICATION_COMP_SERVICE_KEY,
                wsMajorVersion,
                config.getWebServiceDatabase(),
                appRuntimeEnv);

        log.info("LookupKey: " + nofitificationCompServiceLookupKey + "\n");

        try {
            return (NotificationCompService) GenericServiceClientFactory.getService(
                    nofitificationCompServiceLookupKey,
                    config.getNotificationCompUserName(),
                    keyStoreManager.decrypt(config.getNotificationCompPassword()),
                    NotificationCompService.class);

        } catch (Exception ex) {
            log.error("Could not initialize NotificationComp service: " + ex);
            throw new VaadinUIException("Could not initialize NotificationComp service.");
        }
    }
}

