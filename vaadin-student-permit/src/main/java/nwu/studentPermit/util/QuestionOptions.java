package nwu.studentPermit.util;

import java.io.Serializable;

public final class QuestionOptions implements Serializable {

    private static final long serialVersionUID = 1L;

    private final int optionIndex;
    private final String optionText;
    private final String optionValue;
    private int selectedIndex;
    private String selectedText;
    private String selectedValue;
    private String questionID;
    private String questionnaireID;

    public QuestionOptions(String questionnaireID, String questionID, int optionIndex, String optionText, String optionValue) {
        this.questionnaireID = questionnaireID;
        this.questionID = questionID;
        this.optionIndex = optionIndex;
        this.optionText = optionText;
        this.optionValue = optionValue;
    }

    public String getQuestionID() {
        return questionID;
    }

    public String getQuestionnaireID() {
        return questionnaireID;
    }

    public int getOptionIndex() {
        return optionIndex;
    }

    public String getOptionText() {
        return optionText;
    }
   
    public String getOptionValue() {
        return optionValue;
    }

    public int getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(int selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String getSelectedText() {
        return selectedText;
    }

    public void setSelectedText(String selectedText) {
        this.selectedText = selectedText;
    }

    public String getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(String selectedValue) {
        this.selectedValue = selectedValue;
    }

}
