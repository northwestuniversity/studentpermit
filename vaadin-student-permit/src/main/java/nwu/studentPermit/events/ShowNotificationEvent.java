package nwu.studentPermit.events;

import nwu.ac.za.framework.event.UIEvent;
import nwu.ac.za.ui.SeverityType;

public class ShowNotificationEvent extends UIEvent {
    private String errorKey;
    private String errorMessage;
    private SeverityType severityType;
    private Boolean translate;

    public ShowNotificationEvent() {
    }

    public ShowNotificationEvent(String errorMessage, SeverityType severityType) {
        this.errorMessage = errorMessage;
        this.severityType = severityType;
    }

    public String getErrorKey() {
        return errorKey;
    }

    public void setErrorKey(String errorKey) {
        this.errorKey = errorKey;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public SeverityType getSeverityType() {
        return severityType;
    }

    public void setSeverityType(SeverityType severityType) {
        this.severityType = severityType;
    }

    public Boolean getTranslate() {
        translate = errorKey != null && errorMessage == null;

        return translate;
    }
}
